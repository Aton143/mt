# MT single-header library

Small cross-platform multithreading library (for Windows and macOS) written in C/C++ in the style of [stb](https://github.com/nothings/stb).

## Features

As of the latest commit, this library contains intrinsics for atomic compare-and-swap and add instructions, cross-platform thread creation (Windows and POSIX-compliant systems), and ticket mutexes.

Planned features include:
- Cross-platform intrinsics for semaphores, etc.
- Lock-free queue implementation

#include <windows.h>
#include <shlwapi.h>

#define MT_DEBUG
#define MT_IMPLEMENTATION
#include "mt.h"

#define ArrayCount(Array) (sizeof(Array) / sizeof(*Array))
#define MAX_GLOBAL_COUNT 128

mt_global_variable volatile mt_i32 GlobalCount = 0;
mt_global_variable mt_mutex GlobalMutex = {};

void *
ThreadPrintEvens(void *Argument)
{
  DWORD WaitForMutexResult;
  mt_i8 Buffer[256] = {};

  while (GlobalCount < MAX_GLOBAL_COUNT) 
  {
    if ((GlobalCount % 2) == 0) 
    {
      WaitForMutexResult = WaitForSingleObject(GlobalMutex.Handle, INFINITE);  
      if (WaitForMutexResult == WAIT_OBJECT_0)
      {
        wsprintf(Buffer, "Thread 1 (Evens): %u\n", GlobalCount);
        OutputDebugStringA(Buffer);

        GlobalCount++; // Atomic add and fence?

        ReleaseMutex(GlobalMutex.Handle);
      }
    }
  }

  return(Argument);
}

void *
ThreadPrintOdds(void *Argument)
{
  DWORD WaitForMutexResult;
  mt_i8 Buffer[256] = {};

  while (GlobalCount < MAX_GLOBAL_COUNT) 
  {
    if ((GlobalCount % 2) == 1) 
    {
      WaitForMutexResult = WaitForSingleObject(GlobalMutex.Handle, INFINITE);  
      if (WaitForMutexResult == WAIT_OBJECT_0)
      {
        wsprintf(Buffer, "Thread 2 (Odds) : %u\n", GlobalCount);
        OutputDebugStringA(Buffer);

        GlobalCount++; // Atomic add and fence?

        ReleaseMutex(GlobalMutex.Handle);
      }
    }
  }

  return(Argument);
}

int
WinMain(HINSTANCE Instance,
        HINSTANCE PreviousInstance,
        LPSTR CommandLine,
        int ShowCommand)
{
  MT_UNUSED(Instance);
  MT_UNUSED(PreviousInstance);
  MT_UNUSED(CommandLine);
  MT_UNUSED(ShowCommand);

  mt_thread_handle ThreadInfo[2] = {};

  ThreadInfo[0] = MT_CreateThread(NULL, ThreadPrintEvens, NULL);
  ThreadInfo[1] = MT_CreateThread(NULL, ThreadPrintOdds, NULL);

  GlobalMutex = MT_CreateMutex();

  while (GlobalCount < MAX_GLOBAL_COUNT) {}

  return(0);
}


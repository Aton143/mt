#include <windows.h>
#include <shlwapi.h>

#define MT_DEBUG
#define MT_IMPLEMENTATION

#include "mt.h"

struct win32_thread_information
{
  HANDLE SemaphoreHandle;
  mt_i32 LogicalThreadIndex;
};

struct work_queue_entry
{
  char *StringToPrint;
};

static mt_u32 volatile EntryCompletionCount;
static mt_u32 volatile NextEntryToDo;
static mt_u32 volatile EntryCount;
static work_queue_entry Entries[256];

void
PushString(char *String, HANDLE SemaphoreHandle)
{
  MT_ASSERT(EntryCount < 256);

  work_queue_entry *Entry = Entries + EntryCount;
  Entry->StringToPrint = String;

  MT_COMPLETE_PREVIOUS_WRITES_BEFORE_FUTURE_WRITES;

  ++EntryCount;

  // 
  ReleaseSemaphore(SemaphoreHandle, 1, NULL);
}

mt_inline mt_bool
DoWorkerWork(mt_u32 LogicalThreadIndex)
{
  mt_bool DidSomeWork = false;
  if (NextEntryToDo < EntryCount)
  {
    mt_u32 EntryIndex = MT_AtomicAddU32((mt_u32 volatile *) &NextEntryToDo, 1);

    MT_COMPLETE_PREVIOUS_READS_BEFORE_FUTURE_READS;

    work_queue_entry *Entry = Entries + EntryIndex;

    mt_i8 Buffer[256] = {};
    wsprintf(Buffer, "Thread %u: %s\n", LogicalThreadIndex, Entry->StringToPrint);
    OutputDebugStringA(Buffer);

    MT_AtomicAddU32((mt_u32 volatile *) &EntryCompletionCount, 1);
    DidSomeWork = true;
  }

  return(DidSomeWork);
}

void *
ThreadProcedure(void *Argument)
{
  win32_thread_information *ThreadInfo = (win32_thread_information *) Argument;

  for (;;)
  {
    if (!DoWorkerWork(ThreadInfo->LogicalThreadIndex))
    {
      WaitForSingleObjectEx(ThreadInfo->SemaphoreHandle, INFINITE, FALSE);
    }
  }
  
  // return(NULL);
}

int
WinMain(HINSTANCE hInstance,
        HINSTANCE hPrevInstance,
        LPSTR lpCmdLine,
        int nShowCmd)
{
  MT_UNUSED(hInstance);
  MT_UNUSED(hPrevInstance);
  MT_UNUSED(lpCmdLine);
  MT_UNUSED(nShowCmd);

  mt_cpu_information CPUInfo = MT_GetCPUInformation();
  mt_u32 CoreCount = CPUInfo.PhysicalCoreCount;
  win32_thread_information *ThreadInfo = (win32_thread_information *) calloc(CoreCount, sizeof(*ThreadInfo));

  HANDLE SemaphoreHandle =
    CreateSemaphoreEx(NULL, 0, CPUInfo.PhysicalCoreCount, NULL, 0, SEMAPHORE_ALL_ACCESS);

  for (mt_u32 ThreadIndex = 0;
       ThreadIndex < CoreCount - 1;
       ++ThreadIndex)
  {
    win32_thread_information *Info = ThreadInfo + ThreadIndex;

    Info->SemaphoreHandle = SemaphoreHandle;
    Info->LogicalThreadIndex = ThreadIndex;

    MT_CreateThread(NULL, ThreadProcedure, (void *) Info);
  }

  PushString("String 0", SemaphoreHandle);
  PushString("String 1", SemaphoreHandle);
  PushString("String 2", SemaphoreHandle);
  PushString("String 3", SemaphoreHandle);
  PushString("String 4", SemaphoreHandle);
  PushString("String 5", SemaphoreHandle);
  PushString("String 6", SemaphoreHandle);
  PushString("String 7", SemaphoreHandle);
  PushString("String 8", SemaphoreHandle);
  PushString("String 9", SemaphoreHandle);

  while (EntryCount != EntryCompletionCount)
  {
    DoWorkerWork(CoreCount - 1);
  }

  return(0);
}

/* 
 * MT (v0.1): Multithreading, a simple, in-the-making cross-platform multithreading library
 * by Aton143 (Antonio Martinez Arias - http://antoniomartinezarias.com)
 *
 * To use the libary-defined functions, make sure to define MT_IMPLEMENTATION
 * somewhere in your code BEFORE including this file
 * (like the stb libraries by nothings [https://github.com/nothings/stb])
 *
 * ...Your code here...
 *
 * #define MT_IMPLEMENTATION
 * #include "mt.h"
 *
 * ...Your code here...
 * MT_AtomicCompareAndSwapI32(..., ..., ...);
 * ...Your code here...
 *
 * See win32_mt.cpp for a more in-depth example
 */

/* TODO(antonio): Ensure that the operations are type-safe */

#ifndef MT_INCLUDE_MT_H
#define MT_VERSION 1

typedef char          mt_i8;
typedef unsigned char mt_u8;

/* defining core types */
#ifdef _MSC_VER
typedef __int32          mt_i32;
typedef __int64          mt_i64;

typedef unsigned __int32 mt_u32;
typedef unsigned __int64 mt_u64;

typedef SIZE_T           mt_size_t;

typedef struct
{
  DWORD ThreadId;
} mt_thread_handle;

typedef struct
{
  HANDLE Handle;
} mt_mutex;

#else

/*
 * NOTE(antonio): This is assuming we are a POSIX-compliant system.
 * Compile and link with -pthread
 */

#include <pthread.h>
#include <stddef.h>
#include <stdint.h>

typedef int32_t  mt_i32;
typedef int64_t  mt_i64;

typedef uint32_t mt_u32;
typedef uint64_t mt_u64;

typedef size_t   mt_size_t;

typedef struct
{
  pthread_t Handle;
  int       ReturnCode;
} mt_thread_handle;

#endif

#include <stdbool.h>
typedef bool mt_bool;

#ifdef __cplusplus
extern "C"
{
#endif

#ifndef MT_DEF
  #ifdef MT_STATIC
    #define MT_DEF static
  #else
    #define MT_DEF extern
  #endif
#endif

/*
 * NOTE: This library will round up to the nearest rounding size AND add a guard page at the end
 * 
 * Since Windows reserves the stack size minus one page (which it uses as the guard page),
 * we will ensure that (if you are not using the default) the total stack size for the thread is
 * the given thread stack size plus a guard page. We will only allocate ONE guard page to 
 * ensure that this behavior is consistent.
 *
 * Source for the above: https://learn.microsoft.com/en-us/windows/win32/procthread/thread-stack-size
 *
 * Since Windows rounds up by 1 MB when the requested thread stack size is greater than the default,
 * we will follow this policy. I get it, but consistency is important at this stage for me.
 *
 * The above implies that the default stack size on Windows (1 MB) plus the guard page 
 *
 * I am also currently avoiding the topic of setting the stack address on Windows :(
 */

#define MT_KILOBYTES(N) ((N) * (1LL * 1024))
#define MT_MEGABYTES(N) (MT_KILOBYTES(N) * (1LL * 1024))

typedef struct
{
  mt_i32 PhysicalCoreCount;
  mt_i32 LogicalCoreCount;
} mt_cpu_information;

/*
 * Gets CPU information as defined above
 * NOTE: this succeeds if the physical core count is non-zero
 * otherwise, will always return logical core count
 */

MT_DEF mt_cpu_information
MT_GetCPUInformation();

typedef struct
{
  mt_size_t ThreadStackSize;
  mt_bool   AddGuardPageOnTopOfThreadStackSize;

  // NOTE(antonio): This has to be allocated by the caller
  void     *POSIX_AllocatedStackStart;
} mt_thread_creation_options;

/*
 * Creates a thread with standard security
 * NOTE: Not tested nor really implemented on POSIX
 *
 * The ThreadEntryFunction (void * -> void *) will be brokered through a ThreadProc function
 * which must return a DWORD (unsigned 32-bit int)
 *
 * For now, we will return 0 in that function and assume that you do NOT need that return value.
 * I know, this is a shaky decision at best, but I've got get this up and running!
 *
 */

MT_DEF mt_thread_handle
MT_CreateThread(mt_thread_creation_options *ThreadCreationOptions,
                void *(*ThreadEntryFunction)(void *), void *ThreadEntryFunctionArgument);

/*
 * NOTE: These operations will generate full memory barriers (read/write)
 * and need to be aligned according to their sizes (32-bit and 64-bit) 
 */

/*
 * All of the following return the initial value at the DestAddress parameter
 */

MT_DEF mt_i32
MT_AtomicCompareAndSwapI32(mt_i32 volatile *DestAddress, mt_i32 Expected, mt_i32 New);

MT_DEF mt_u32
MT_AtomicCompareAndSwapU32(mt_u32 volatile *DestAddress, mt_u32 Expected, mt_u32 New);

MT_DEF mt_i64
MT_AtomicCompareAndSwapI64(mt_i64 volatile *DestAddress, mt_i64 Expected, mt_i64 New);

MT_DEF mt_u64
MT_AtomicCompareAndSwapU64(mt_u64 volatile *DestAddress, mt_u64 Expected, mt_u64 New);

MT_DEF void *
MT_AtomicCompareAndSwapPointer(void volatile **DestAddress, void *Expected, void *New);

MT_DEF mt_i32
MT_AtomicAddI32(mt_i32 volatile *DestAddress, mt_i32 ValueToAdd);

MT_DEF mt_u32
MT_AtomicAddU32(mt_u32 volatile *DestAddress, mt_u32 ValueToAdd);

MT_DEF mt_i64
MT_AtomicAddI64(mt_i64 volatile *DestAddress, mt_i64 ValueToAdd);

MT_DEF mt_u64
MT_AtomicAddU64(mt_u64 volatile *DestAddress, mt_u64 ValueToAdd);

typedef struct
{
  mt_u64 volatile Ticket;
  mt_u64 volatile Serving;
} mt_ticket_mutex;

/* enter a critical section using a FIFO */
MT_DEF void
MT_BeginTicketMutex(mt_ticket_mutex *Mutex);

/* exit a critical section using a FIFO */
MT_DEF void
MT_EndTicketMutex(mt_ticket_mutex *Mutex);

MT_DEF mt_mutex
MT_CreateMutex(void);

/*
typedef struct mt_work_queue_entry mt_work_queue_entry;

typedef struct
{

} mt_work_queue;
*/

#if __cplusplus
}
#endif

#define MT_INCLUDE_MT_H
#endif /* MT_INCLUDE_MT_H */

#ifdef MT_IMPLEMENTATION

#ifndef mt_global_variable
  #define mt_global_variable static 
#endif

#ifndef MT_ZERO_STRUCT
  #ifdef __cplusplus
    #define MT_ZERO_STRUCT {}
  #else
    #define MT_ZERO_STRUCT {0}
  #endif
#endif

/* MT_ASSERT define */
#ifndef MT_ASSERT
  #ifdef MT_DEBUG
    #define Assert(X) do {if (!(X)) {*((int *) 0) = 0;}} while(0)
  #else
    #define Assert(X)
  #endif

  #define MT_ASSERT(x) Assert(x)
#endif

/* MT_EMPTY_BLOCK define */
#ifndef MT_EMPTY_BLOCK
  #define MT_EMPTY_BLOCK int i##__LINE__ = 0
#endif 

/* MT_EXTERN define */
#ifdef __cplusplus
  #define  MT_EXTERN extern "C"
#else
  #define  MT_EXTERN extern
#endif

/* mt_inline define */
#ifndef _MSC_VER
  #ifdef __cplusplus
    #define mt_inline inline
  #else
    #define mt_inline
  #endif
#else
  #define mt_inline __forceinline
#endif 

/* MT_THREAD_LOCAL define */
#if defined(__cplusplus) && __cplusplus >= 201103L
  #define MT_THREAD_LOCAL       thread_local
#elif defined(__GNUC__) && __GNUC__ < 5
  #define MT_THREAD_LOCAL       __thread
#elif defined(_MSC_VER)
  #define MT_THREAD_LOCAL       __declspec(thread)
#elif defined (__STDC_VERSION__) && __STDC_VERSION__ >= 201112L
  #define MT_THREAD_LOCAL       _Thread_local
#endif

#ifndef MT_THREAD_LOCAL
  #if defined(__GNUC__)
    #define MT_THREAD_LOCAL     __thread
  #endif
#endif

/* should produce compiler error if size is wrong */
typedef unsigned char ValidateU32[sizeof(mt_u32) == 4 ? 1 : -1];

/* MT_UNUSED */
#ifdef _MSC_VER
  #define MT_UNUSED(V) (void)(V)
#else
  #define MT_UNUSED(V) (void)sizeof(V)
#endif

#ifndef MT_SPIN
  /* TODO(antonio): Verify efficacy of this */
  #define MT_SPIN(Expression) while(Expression)
#endif

mt_u32
MT_SlowCountBitsU64(mt_u64 Value)
{
  mt_u32 Result = 0;

  while (Value)
  {
    Value &= (Value - 1);
    Result++;
  }

  return(Result);
}

/* function implementations for MSVC */
#ifdef _MSC_VER
/* TODO(antonio): Support for those who do not want intrinsics */
#include <intrin.h>

#if !defined(MT_COMPLETE_PREVIOUS_READS_BEFORE_FUTURE_READS) && !defined(MT_COMPLETE_PREVIOUS_WRITES_BEFORE_FUTURE_WRITES)
  #define MT_COMPLETE_PREVIOUS_READS_BEFORE_FUTURE_READS _ReadBarrier()
  #define MT_COMPLETE_PREVIOUS_WRITES_BEFORE_FUTURE_WRITES _WriteBarrier()
  #define MT_COMPLETE_PREVIOUS_WRITES_AND_READS_BEFORE_FUTURE_WRITES_AND_READS do {_ReadBarrier(); _WriteBarrier()} while(0)
#endif

mt_inline MT_DEF mt_cpu_information
MT_GetCPUInformationFromSystemInformation()
{
  mt_cpu_information Result = MT_ZERO_STRUCT;

  SYSTEM_INFO SystemInformation;
  GetSystemInfo(&SystemInformation);

  Result.LogicalCoreCount = (mt_u32) SystemInformation.dwNumberOfProcessors;
  return(Result);
}

mt_inline MT_DEF mt_u64
MT_GetPageSize()
{
  SYSTEM_INFO SystemInformation;
  GetSystemInfo(&SystemInformation);

  mt_i64 PageSize = (mt_u64) SystemInformation.dwPageSize;
  return(PageSize);
}

mt_inline MT_DEF mt_u64
MT_GetAllocationGranularity()
{
  SYSTEM_INFO SystemInformation;
  GetSystemInfo(&SystemInformation);

  mt_i64 AllocationGranularity = 
    (mt_i64) SystemInformation.dwAllocationGranularity;
  return(AllocationGranularity);
}

#ifndef STACKSIZE
  #define MT_DEFAULT_STACKSIZE MT_MEGABYTES(1)
 #elif
  #define MT_DEFAULT_STACKSIZE STACKSIZE
#endif

#define MT_MEGABYTE_MASK 0xFFFFF

MT_DEF void
MT_PrintSystemError()
{
  LPTSTR ErrorBuffer = NULL;
  DWORD Error = GetLastError();
  FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_IGNORE_INSERTS,
                NULL, Error, 0, (LPTSTR) &ErrorBuffer, 0, NULL);
  OutputDebugStringA(ErrorBuffer);
}

/*
 * TODO(antonio): Clean up the following functions
 */
mt_inline MT_DEF mt_bool 
MT_IsPowerOfTwoU64(mt_u64 Value)
{
  mt_bool Result = (__popcnt64(Value) == 1);
  return(Result);
}

mt_inline MT_DEF mt_u32
MT_CountSetBitsU64(mt_u64 Value)
{
  mt_u32 Result = (mt_u32) __popcnt64(Value);
  return(Result);
}

mt_inline MT_DEF mt_bool
MT_IsThreadHandleValid(mt_thread_handle *ThreadHandle)
{
  mt_bool Result = (ThreadHandle->ThreadId != 0);
  return(Result);
}

#define GET_LOGICAL_PROCESSOR_INFORMATION(name) \
  BOOL name(PSYSTEM_LOGICAL_PROCESSOR_INFORMATION Buffer, PDWORD ReturnedLength)
typedef GET_LOGICAL_PROCESSOR_INFORMATION(get_logical_processor_information);
GET_LOGICAL_PROCESSOR_INFORMATION(GetLogicalProcessorInformationStub)
{
  MT_UNUSED(Buffer);
  MT_UNUSED(ReturnedLength);
  return(FALSE);
}
mt_global_variable get_logical_processor_information *
GetLogicalProcessorInformation_ = GetLogicalProcessorInformationStub;

#define GetLogicalProcessorInformation GetLogicalProcessorInformation_

MT_DEF mt_cpu_information
MT_GetCPUInformation()
{
  mt_cpu_information Result = MT_ZERO_STRUCT;

  PSYSTEM_LOGICAL_PROCESSOR_INFORMATION LogicalProcessorInformationBuffer = NULL;
  DWORD LogicalProcessorInformationBufferLength = 0;

  HMODULE KernelLibModuleHandle = LoadLibraryA("kernel32.dll");
  if (KernelLibModuleHandle)
  {
    GetLogicalProcessorInformation = (get_logical_processor_information *) GetProcAddress(KernelLibModuleHandle, "GetLogicalProcessorInformation");

    if(GetLogicalProcessorInformation)
    {
      mt_bool ObtainingLogicalProcessorInformation = true;
      while(ObtainingLogicalProcessorInformation)
      {
        BOOL ReturnCode = GetLogicalProcessorInformation(LogicalProcessorInformationBuffer, &LogicalProcessorInformationBufferLength);

        if (ReturnCode == FALSE)
        {
          DWORD GetLogicalProcessorInformationErrorCode = GetLastError();

          if (GetLogicalProcessorInformationErrorCode == ERROR_INSUFFICIENT_BUFFER)
          {
            /* NOTE(antonio): Since we only need this for this function call, stack alloc */
            LogicalProcessorInformationBuffer =
              (PSYSTEM_LOGICAL_PROCESSOR_INFORMATION) _alloca((mt_size_t) LogicalProcessorInformationBufferLength);

            if (LogicalProcessorInformationBuffer == NULL)
            {
              Result = MT_GetCPUInformationFromSystemInformation();
              return(Result);
            }
          }
          else
          {
            MT_PrintSystemError();
          }
        }
        else
        {
          ObtainingLogicalProcessorInformation = false;
        }
      }
    }
    else
    {
      Result = MT_GetCPUInformationFromSystemInformation();
      return(Result);
    }
  }
  else
  {
    Result = MT_GetCPUInformationFromSystemInformation();
    return(Result);
  }

  PSYSTEM_LOGICAL_PROCESSOR_INFORMATION LogicalProcessorInformationPointer = LogicalProcessorInformationBuffer;
  DWORD Offset = 0;
  mt_i32 PhysicalCoreCount = 0;
  mt_i32 LogicalCoreCount = 0;

  while ((Offset + sizeof(LogicalProcessorInformationPointer)) <= LogicalProcessorInformationBufferLength)
  {
    if (LogicalProcessorInformationPointer->Relationship == RelationProcessorCore)
    {
      PhysicalCoreCount++;
      LogicalCoreCount += MT_CountSetBitsU64((mt_u64) LogicalProcessorInformationPointer->ProcessorMask);
    }

    Offset += sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION);
    LogicalProcessorInformationPointer++;
  }

  Result.PhysicalCoreCount = PhysicalCoreCount;
  Result.LogicalCoreCount = LogicalCoreCount;

  return(Result);
}

typedef struct
{
  volatile void *(*Function)(void *);
  volatile void *Argument;
  HANDLE StartedTaskSemaphore;
} mt_win32_thread_creation_function_wrapper;

DWORD WINAPI ThreadProc(LPVOID EntryParameter)
{
  mt_win32_thread_creation_function_wrapper *FunctionCallWrapper =
    (mt_win32_thread_creation_function_wrapper *) EntryParameter;

  /*
   * Get function from memory before the calling thread changes it
   */
  volatile void *(*ThreadFunction)(void *) = FunctionCallWrapper->Function;
  volatile void *Argument = FunctionCallWrapper->Argument;
  HANDLE Semaphore = FunctionCallWrapper->StartedTaskSemaphore;

  ReleaseSemaphore(Semaphore, 1, 0);

  ThreadFunction((void *) Argument);

  return(0);
};

#define MT_DEFAULT_THREAD_OPTIONS {MT_DEFAULT_STACKSIZE, true, NULL}

MT_DEF mt_thread_handle
MT_CreateThread(mt_thread_creation_options *ThreadCreationOptions,
                void *(*ThreadEntryFunction)(void *),
                void *ThreadEntryFunctionArgument)
{
  mt_thread_handle Result = MT_ZERO_STRUCT;

  mt_thread_creation_options Options = MT_DEFAULT_THREAD_OPTIONS; 

  if (ThreadCreationOptions)
  {
    if (ThreadCreationOptions->ThreadStackSize == 0)
    {
      Options.ThreadStackSize = MT_DEFAULT_STACKSIZE;
    }

    // NOTE(antonio): Add the page size for the guard page if requested
    if (ThreadCreationOptions->AddGuardPageOnTopOfThreadStackSize)
    {
      Options.ThreadStackSize += (mt_size_t) MT_GetPageSize();
    }

    if (ThreadCreationOptions->ThreadStackSize < MT_DEFAULT_STACKSIZE)
    {
      mt_u64 AllocationGranularity = MT_GetAllocationGranularity();

      if (MT_IsPowerOfTwoU64(AllocationGranularity))
      {
        mt_u64 Mask = AllocationGranularity - 1;
        mt_u64 Masked = ThreadCreationOptions->ThreadStackSize & Mask;
        mt_u64 RoundedUpToNextGranularity = ThreadCreationOptions->ThreadStackSize & ~Mask;

        if (Masked != 0)
        {
          RoundedUpToNextGranularity += AllocationGranularity;
        }

        Options.ThreadStackSize = RoundedUpToNextGranularity;
      }
    }
    else if (ThreadCreationOptions->ThreadStackSize > MT_DEFAULT_STACKSIZE)
    {
      mt_size_t Masked = ThreadCreationOptions->ThreadStackSize & MT_MEGABYTE_MASK;
      mt_size_t RoundedUpToNextMB = ThreadCreationOptions->ThreadStackSize & ~MT_MEGABYTE_MASK;

      if (Masked != 0)
      {
        RoundedUpToNextMB += MT_MEGABYTES(1);
      }

      Options.ThreadStackSize = RoundedUpToNextMB;
    }
  }

  HANDLE SemaphoreHandle = CreateSemaphoreEx(NULL, 0, 1, NULL, 0, SEMAPHORE_ALL_ACCESS);

  mt_win32_thread_creation_function_wrapper FunctionCallWrapper = MT_ZERO_STRUCT;

  FunctionCallWrapper.Function = (volatile void *(__cdecl *)(void *)) ThreadEntryFunction;
  FunctionCallWrapper.Argument = (volatile void *) ThreadEntryFunctionArgument;
  FunctionCallWrapper.StartedTaskSemaphore = SemaphoreHandle;

  /* TODO(antonio): Make sure that the thread calls the function before leaving */
  HANDLE ThreadHandle = CreateThread(NULL, Options.ThreadStackSize,
                                     ThreadProc,
                                     (void *) &FunctionCallWrapper, 0, &Result.ThreadId);

  if (ThreadHandle == NULL)
  {
    /* Error handling */
    DWORD LastError = GetLastError();
    MT_UNUSED(LastError);
  }
  else
  {
    /* NOTE(antonio): Ensure that the user-provided thread is called before leaving the function */
    DWORD WaitResult = WaitForSingleObjectEx(SemaphoreHandle, INFINITE, FALSE);
    MT_UNUSED(WaitResult);

    CloseHandle(SemaphoreHandle);
  }

  CloseHandle(ThreadHandle);

  return(Result);
}

MT_DEF mt_i32
MT_AtomicCompareAndSwapI32(mt_i32 volatile *DestAddress,
                           mt_i32 Expected,
                           mt_i32 New)
{
  mt_i32 Result = _InterlockedCompareExchange((volatile LONG *) DestAddress, (LONG) New, (LONG) Expected);
  return(Result);
}

MT_DEF mt_u32
MT_AtomicCompareAndSwapU32(mt_u32 volatile *DestAddress,
                           mt_u32 Expected,
                           mt_u32 New)
{
  mt_u32 Result = (mt_u32) _InterlockedCompareExchange((volatile LONG *) DestAddress, (LONG) New, (LONG) Expected);
  return(Result);
}

MT_DEF mt_i64
MT_AtomicCompareAndSwapI64(mt_i64 volatile *DestAddress,
                           mt_i64 Expected,
                           mt_i64 New)
{
  mt_i64 Result = _InterlockedCompareExchange64((volatile __int64 *) DestAddress, (__int64) New, (__int64) Expected);
  return(Result);
}

MT_DEF mt_u64
MT_AtomicCompareAndSwapU64(mt_u64 volatile *DestAddress,
                           mt_u64 Expected,
                           mt_u64 New)
{
  mt_u64 Result = (mt_u64) _InterlockedCompareExchange64((volatile __int64 *) DestAddress, New, Expected);
  return(Result);
}

MT_DEF void *
MT_AtomicCompareAndSwapPointer(void volatile **DestAddress,
                               void *Expected,
                               void *New)
{
  void *Result = _InterlockedCompareExchangePointer((volatile PVOID *) DestAddress, New, Expected);
  return(Result);
}

MT_DEF mt_i32
MT_AtomicAddI32(mt_i32 volatile *DestAddress,
                mt_i32 ValueToAdd)
{
  mt_i32 Result = _InterlockedExchangeAdd((volatile LONG *) DestAddress, ValueToAdd);
  return(Result);
}

MT_DEF mt_u32
MT_AtomicAddU32(mt_u32 volatile *DestAddress,
                mt_u32 ValueToAdd)
{
  mt_u32 Result = (mt_u32) _InterlockedExchangeAdd((volatile LONG *) DestAddress, ValueToAdd);
  return(Result);
}

MT_DEF mt_i64
MT_AtomicAddI64(mt_i64 volatile *DestAddress,
                mt_i64 ValueToAdd)
{
  mt_i64 Result = _InterlockedExchangeAdd64((volatile __int64 *) DestAddress, ValueToAdd);
  return(Result);
}

MT_DEF mt_u64
MT_AtomicAddU64(mt_u64 volatile *DestAddress,
                mt_u64 ValueToAdd)
{
  mt_u64 Result = (mt_u64) _InterlockedExchangeAdd64((volatile __int64 *) DestAddress, ValueToAdd);
  return(Result);
}

MT_DEF mt_mutex
MT_CreateMutex(void)
{
  mt_mutex Mutex = MT_ZERO_STRUCT;
  Mutex.Handle = CreateMutexExA(NULL, /* Security attributes */
                                NULL, /* Name */
                                0,    /* Owner flags */
                                MUTEX_ALL_ACCESS); /* Access flags */
  return(Mutex);
}

#endif /* end function implementations for MSVC */

/* TODO(antonio): these need to be tested */
/* function implementations for MacOS */
#if defined(__APPLE__) && defined(__MACH__)
#include <libkern/OSAtomic.h>
#include <unistd.h>

#ifndef MT_GetPageSize()
  #if defined(_SC_PAGESIZE)
    #define MT_GetPageSize() sysconf(_SC_PAGESIZE)
  #elif defined(_SC_PAGE_SIZE)
    #define MT_GetPageSize() sysconf(_SC_PAGE_SIZE)
  #else
    #define MT_GetPageSize() getpagesize()
  #endif
#endif

#define MT_DEFAULT_THREAD_CREATION_OPTIONS \
  (mt_thread_creation_options) {0, true, NULL}

MT_DEF mt_thread_handle
MT_CreateThread(mt_thread_creation_options *ThreadCreationOptions,
                void *(*ThreadEntryFunction)(void *),
                void *ThreadEntryFunctionArgument);
{
  mt_thread_handle Result = MT_ZERO_STRUCT;

  /*
   * NOTE: we do not set the following attributes:
   * detachstate
   * affinity_np (though this could be helpful)
   * guardsize (Windows sets this as the last page in the stack)
   * schedpolicy
   * schedparam
   * scope (since not even Linux implements both options, apparently)
   * sigmask_np
   */ 

  pthread_attr_t ThreadAttributes;
  if (pthread_attr_init(&ThreadAttributes) == 0)
  {
    Result.ReturnCode = 
      pthread_create(&Result.Handle, NULL, ThreadEntryFunction, ThreadEntryFunctionArgument);

    if (Result.ReturnCode)
    {
      // TODO(antonio): Handle the error
      MT_EMPTY_BLOCK;
    }

    pthread_attr_destroy(&ThreadAttributes);
  }

  return(Result);
}

/* NOTE(antonio): OSAtomic.h does not provide a separate read and write barrier */
#if !defined(MT_COMPLETE_PREVIOUS_READS_BEFORE_FUTURE_READS) && !defined(MT_COMPLETE_PREVIOUS_WRITES_BEFORE_FUTURE_WRITES)
  #define MT_COMPLETE_PREVIOUS_READS_BEFORE_FUTURE_READS OSMemoryBarrier()
  #define MT_COMPLETE_PREVIOUS_WRITES_BEFORE_FUTURE_WRITES OSMemoryBarrier()
  #define MT_COMPLETE_PREVIOUS_WRITES_AND_READS_BEFORE_FUTURE_WRITES_AND_READS OSMemoryBarrier()
#endif

MT_DEF mt_i32
MT_AtomicCompareAndSwapI32(mt_i32 volatile *DestAddress,
                           mt_i32 Expected,
                           mt_i32 New)
{
 mt_bool CASResult = OSAtomicCompareAndSwap32Barrier(Expected, New, DestAddress);
 mt_u32 Result = CASResult ? Expected : New;
 return(Result);
}

MT_DEF mt_u32
MT_AtomicCompareAndSwapU32(mt_u32 volatile *DestAddress,
                           mt_u32 Expected,
                           mt_u32 New)
{
 mt_bool CASResult = OSAtomicCompareAndSwap32Barrier(Expected, New, (uint32_t volatile *) DestAddress);
 mt_u32 Result = CASResult ? Expected : New;
 return(Result);
}

MT_DEF mt_i64
MT_AtomicCompareAndSwapI64(mt_i64 volatile *DestAddress,
                           mt_i64 Expected,
                           mt_i64 New)
{
  mt_bool CASResult = OSAtomicCompareAndSwap64Barrier(Expected, New, DestAddress);
  mt_i64 Result = CASResult ? Expected : New;
  return(Result);
}

MT_DEF mt_u64
MT_AtomicCompareAndSwapU64(mt_u64 volatile *DestAddress,
                           mt_u64 Expected,
                           mt_u64 New)
{
  mt_bool CASResult = OSAtomicCompareAndSwap64Barrier(Expected, New, (int64_t volatile *) DestAddress);
  mt_u64 Result = CASResult ? Expected : New;
  return(Result);
}

MT_DEF void *
MT_AtomicCompareAndSwapPointer(void volatile **DestAddress,
                               void *Expected,
                               void *New)
{
  mt_bool CASResult = OSAtomicCompareAndSwapPtrBarrier(Expected, New, DestAddress);
  void *Result = CASResult ? Expected : New;
  return(Result);
}

MT_DEF mt_i32
MT_AtomicAddI32(mt_i32 volatile *DestAddress,
                mt_i32 ValueToAdd)
{
  mt_i32 Result = OSAtomicAdd32Barrier(ValueToAdd, DestAddress);
  return(Result);
}

MT_DEF mt_u32
MT_AtomicAddU32(mt_u32 volatile *DestAddress, mt_u32 ValueToAdd)
{
  mt_u32 Result = (mt_u32) OSAtomicAdd32Barrier(ValueToAdd, (int32_t volatile *) DestAddress);
  return(Result);
}

MT_DEF mt_i64
MT_AtomicAddI64(mt_i64 volatile *DestAddress, mt_i64 ValueToAdd)
{
  mt_i64 Result = OSAtomicAdd64Barrier(ValueToAdd, DestAddress);
  return(Result);
}

gT_DEF mt_u64
MT_AtomicAddU64(mt_u64 volatile *DestAddress, mt_u64 ValueToAdd)
{
  mt_u64 Result = (mt_u64) OSAtomicAdd64Barrier(ValueToAdd, (int64_t volatile *) DestAddress);
  return(Result);
}
#endif

mt_inline MT_DEF void
MT_BeginTicketMutex(mt_ticket_mutex *Mutex)
{
  mt_u64 Ticket = MT_AtomicAddU64(&Mutex->Ticket, 1);
  MT_SPIN(Ticket != Mutex->Serving);
}

mt_inline MT_DEF void
MT_EndTicketMutex(mt_ticket_mutex *Mutex)
{
  MT_AtomicAddU64(&Mutex->Serving, 1);
}

#endif /* MT_IMPLEMENTATION */

/*
 * Copyright (C) 2022 Antonio Martinez Arias
 *
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 */
